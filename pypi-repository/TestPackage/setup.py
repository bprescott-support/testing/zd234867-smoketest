import setuptools,os

setuptools.setup(
    name="testpackage",
    version="1.0.0."+os.environ.get("CI_JOB_ID"),
    author="Example Author",
    author_email="author@example.com",
    description="PyPi smoke test package",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
